package chess;



import static org.junit.Assert.*;
import org.junit.Test;



/*
 * This UnitTest tests a valide and not allowed moves 
 * Testing is similar like a classes in packe chess
 * UnitTest use for testing class game_desk with realize basic rules of game
*/

/**
 *
 * @author vymaztom
 */
public class JUnitTest {
       

    /**
    * testing if is not posible move into position where is figure now 
    */
    @Test
    public void test_move_is_not_posible_twice(){  
        game_desk desk = new game_desk();
        assertTrue(desk.player1.move(1,desk.player2.figurky, desk.getINTdesk(), 4, 1));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(1,desk.player2.figurky, desk.getINTdesk(), 4, 1));
        System.out.println("*********************************");
    }
 
    /**
    * test of white pawn
    */
    @Test
    public void test_moves_of_chessman_U_W(){  
      
        game_desk desk = new game_desk();
        
        
        /*
        * testing of rules of first move of pawn figure
        */
        
        assertTrue(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");
        assertTrue(desk.player1.move(2,desk.player2.figurky, desk.getINTdesk(), 5, 2));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(2,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");
        assertTrue(desk.player1.move(2,desk.player2.figurky, desk.getINTdesk(), 4, 2));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(2,desk.player2.figurky, desk.getINTdesk(), 2, 2));
        System.out.println("*********************************");
        
        /*
        testing of 
        */
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 3, 2));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 4, 2));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 5, 2));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 5, 3));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 5, 4));
        System.out.println("*********************************");
        /*
        move of oposite figure due to check if capture is posible
        */
        assertTrue(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");
        assertTrue(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");  
    }

    /**
    * test of white pawn
    */
    @Test
    public void test_moves_of_chessman_U_B(){
        game_desk desk = new game_desk();
        /*
        testing of first moves
        */
        assertTrue(desk.player2.move(16,desk.player1.figurky, desk.getINTdesk(), 2, 0));
        System.out.println("*********************************");
        assertFalse(desk.player2.move(16,desk.player1.figurky, desk.getINTdesk(), 4, 0));
        System.out.println("*********************************");
        /*
        testing if are posibble valide moves
        */
        assertTrue(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");        
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 2, 4));
        System.out.println("*********************************");       
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 2, 3));
        System.out.println("*********************************");       
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 2, 5));
        System.out.println("*********************************");       
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 3, 3));
        System.out.println("*********************************");       
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 3, 5));
        System.out.println("*********************************");       
        assertFalse(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 4, 5));
        System.out.println("*********************************");       

        /*
        move of oposite figure due to test of attak
        */
        assertTrue(desk.player1.move(3,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");          
        assertTrue(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");    
    }
    /**
     * test of rook
     */
    @Test
    public void test_moves_of_chessman_V(){
        game_desk desk = new game_desk();
        /*
        removing pawns from game
        */
        for(int i = 0 ; i < 8 ; i++){
            desk.player1.figurky[i].active = false;
            desk.player2.figurky[i].active = false;
        }
        /*
        testing moves
        */
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 3, 0));
        System.out.println("*********************************");         
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 3, 7));
        System.out.println("*********************************");         
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 5, 7));
        System.out.println("*********************************");         
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 2, 7));
        System.out.println("*********************************");         
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 2, 3));
        System.out.println("*********************************");         
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 3, 3));
        System.out.println("*********************************");         
        /*
        not posible moves
        */
        assertFalse(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 2, 2));
        System.out.println("*********************************"); 
        assertFalse(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 2, 4));
        System.out.println("*********************************"); 
        assertFalse(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************"); 
        assertFalse(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 4, 2));
        System.out.println("*********************************"); 
        
        /*
        attac of the figure
        */
        assertTrue(desk.player1.move(8,desk.player2.figurky, desk.getINTdesk(), 0, 3));
        System.out.println("*********************************");
        assertFalse(desk.player2.figurky[27-16].active);
    }
    /**
     * testing of knight figure 
     */
    @Test
    public void test_moves_of_chessman_J(){
        game_desk desk = new game_desk();
        /*
        removing pawns from game
        */
        for(int i = 0 ; i < 8 ; i++){
            desk.player1.figurky[i].active = false;
            desk.player2.figurky[i].active = false;
        }
        /*
        testing valide moves
        */
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 3));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 4));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 3));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 2));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 2));
        System.out.println("*********************************");         
        assertFalse(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 2));
        System.out.println("*********************************");
        
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");
        
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 7));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 6));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 6));
        System.out.println("*********************************");      
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 2));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 5, 3));
        System.out.println("*********************************");     
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 4, 1));
        System.out.println("*********************************");
        /*
        testing of attack
        */
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 2, 2));
        System.out.println("*********************************");
        assertTrue(desk.player1.move(14,desk.player2.figurky, desk.getINTdesk(), 0, 1));
        System.out.println("*********************************");        
        assertFalse(desk.player2.figurky[25-16].active);
    }
    /**
     * testing of bishop
     */
    @Test
    public void test_moves_of_chessman_S(){
        game_desk desk = new game_desk();
        /*
        removing pawns from game
        */
        for(int i = 0 ; i < 8 ; i++){
            desk.player1.figurky[i].active = false;
            desk.player2.figurky[i].active = false;
        }
        /*
        testing valide moves
        */    
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 2, 7));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 7, 2));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 5, 0));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 7, 2));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 4, 5));
        System.out.println("*********************************");
        /*
        testing not allowed moves
        */
        assertFalse(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 3, 5));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 6, 4));
        System.out.println("*********************************");
        /*
        testing of attack
        */
        assertTrue(desk.player1.move(10,desk.player2.figurky, desk.getINTdesk(), 0, 1));
        System.out.println("*********************************");
        assertFalse(desk.player2.figurky[25-16].active);
    }
    /**
     * testig a King figure
     */
    @Test
    public void test_moves_of_chessman_K(){
        game_desk desk = new game_desk();
        /*
        removing pawns from game
        */
        for(int i = 0 ; i < 8 ; i++){
            desk.player1.figurky[i].active = false;
            desk.player2.figurky[i].active = false;
        }
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 6, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 5, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");
        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 3, 3));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 3, 5));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 5));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 5, 5));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 5, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 5, 3));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");        
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");        
        /*
        testing of not allowed moves
        */
        assertFalse(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 7, 7));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 1, 0));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 4, 0));
        System.out.println("*********************************");        
        assertFalse(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 6, 4));
        System.out.println("*********************************");        

        /*
        testing of attac
        */
        assertTrue(desk.player2.move(20,desk.player1.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");
        assertTrue(desk.player1.move(12,desk.player2.figurky, desk.getINTdesk(), 3, 4));
        System.out.println("*********************************");
        assertFalse(desk.player2.figurky[20-16].active);
    }
    /**
     * testig a queen figure
     */
    @Test
    public void test_moves_of_chessman_D(){
        game_desk desk = new game_desk();
        /*
        removing pawns from game
        */
        for(int i = 0 ; i < 8 ; i++){
            desk.player1.figurky[i].active = false;
            desk.player2.figurky[i].active = false;
        }
        /*
        testing of valide moves
        */
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 3));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 0));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 7));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 2, 7));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 7));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 2, 2));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 2));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 6, 2));
        System.out.println("*********************************");          
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 4, 4));
        System.out.println("*********************************");          
        /*
        testing of not allowed moves
        */
        assertFalse(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 3, 2));
        System.out.println("*********************************");  
        assertFalse(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 3, 6));
        System.out.println("*********************************");  
        assertFalse(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 5, 6));
        System.out.println("*********************************");  
        assertFalse(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 5, 2));
        System.out.println("*********************************");
        /*
        testing of attac
        */
        assertTrue(desk.player1.move(11,desk.player2.figurky, desk.getINTdesk(), 0, 4));
        System.out.println("*********************************");          
        assertFalse(desk.player2.figurky[28-16].active);
    }
}
