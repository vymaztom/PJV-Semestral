
package chess;

/**
 * figure knight
 * javadoc is same as chessman
 * @author vymaztom
 */
class Chessman_J extends Chessman {

    Chessman_J(int ID, int position_x, int position_y) {
        super(ID, position_x, position_y);
    }
    @Override
    boolean move(Chessman[] oposit, int[][] desk, int x, int y){

        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
        

        
        if((delta_x == 1) && (delta_y == 2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }    
        }
    
        if((delta_x == -1) && (delta_y == 2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }
        if((delta_x == 1) && (delta_y == -2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }
        if((delta_x == -1) && (delta_y == -2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }

        if((delta_x == 2) && (delta_y == 1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }
        if((delta_x == -2) && (delta_y == 1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }
        if((delta_x == 2) && (delta_y == -1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }
        if((delta_x == -2) && (delta_y == -1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){
                        oposit[che - 16].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){
                        oposit[che].active = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }           
            }else{
                this.set_position(x, y);
                return true;
            }        
        }  
        return false;
    }
    
    @Override
    boolean posible_move(Chessman[] oposit, int[][] desk, int x, int y){

        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
        
        //System.out.printf("x = %d , y = %d \n", x,y);
        //System.out.printf("dx = %d , dy = %d \n", delta_x,delta_y);
        
        if((delta_x == 1) && (delta_y == 2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }    
        }
    
        if((delta_x == -1) && (delta_y == 2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }
        if((delta_x == 1) && (delta_y == -2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }
        if((delta_x == -1) && (delta_y == -2)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }

        if((delta_x == 2) && (delta_y == 1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }
        if((delta_x == -2) && (delta_y == 1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }
        if((delta_x == 2) && (delta_y == -1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }
        if((delta_x == -2) && (delta_y == -1)){
            if(desk[x][y] != -1){
                            
                int che = desk[x][y];
                
                if (this.ID < 16){
                    if(che > 15){

                        return true;
                    }
                    return false;
                }else{
                    if(che < 16){

                        return true;
                    }
                    return false;
                }           
            }else{

                return true;
            }        
        }  
        return false;
    }
    
   

}