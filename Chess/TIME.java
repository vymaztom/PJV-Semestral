
package chess;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;





class TIME {
   
    boolean time_switch = true;
    boolean thread_active = true;
    
    long time_actual;
    long time_last = 0;
    
    long end_time;
    long time_player1 = 0;
    long time_player2 = 0;
    
    long time_player1_act = 0;
    long time_player2_act = 0;
    
    long time_actual_act;
    long time_last_act = 0;
    
    TIME_Thread time;
    button_Thread but;
    
    GUI_start_menu frame_gui;
    private final Logger LOGGER;
    
    
    TIME(long end_time, long time_player1, long time_player2, GUI_start_menu frame, JButton b_player1, JButton b_player2, Logger LOGGER){
        this.end_time = end_time;
        this.time_player1 = time_player1;
        this.time_player2 = time_player2;
        this.frame_gui = frame;
        this.time = new TIME_Thread();
        this.but = new button_Thread(b_player1,b_player2);
        this.LOGGER = LOGGER;
        this.time.setPriority(Thread.MAX_PRIORITY);
        this.but.setPriority(Thread.MAX_PRIORITY);
    }
   
    
    void exchange(){
        if(this.time_switch){
            this.time_player1 = this.time_player1 + this.time_actual - this.time_last;
            this.time_last = this.time_actual;
            this.time_switch = false;
        }else{
            this.time_player2 = this.time_player2 + this.time_actual - this.time_last;
            this.time_last = this.time_actual;            
            this.time_switch = true;
        }
    }
    
    void getTime(){
        if(this.time_switch){
            this.time_player1_act = this.time_player1_act + this.time_actual - this.time_last_act;
            this.time_last_act = this.time_actual;
            
            
        }else{
            this.time_player2_act = this.time_player2_act + this.time_actual - this.time_last_act;
            this.time_last_act = this.time_actual;            
            
        }
    }
    
    void setTimeForPlayers(long time_player1,long time_player2){
        this.time_player1 = time_player1;
        this.time_player2 = time_player2;
    }
    
    void setENDtime(long end_time){
        this.end_time = end_time;
    }
    
    class button_Thread extends Thread{

        
        JButton b1;
        JButton b2;
        
        int time1;
        int time2;
        int time1_des;
        int time2_des;
        
        button_Thread(JButton b1, JButton b2){
            
            this.b1 = b1;
            this.b2 = b2;
            //setDaemon(true);
        }
    
        @Override
        public void run(){
            Date date = new Date();
            Date date_end = new Date();

            while(thread_active){
                getTime();
                if(time_player1_act < 60*1000){
                    time1 = (int)(time_player1_act/1000.0);
                    time1_des = (int)(time_player1_act/100.0);
                    time1_des = (time1_des*10 - time1)%10;
                    b2.setText("Wite - " + time1 + "." + time1_des + " sec" );
                }else{
                    time1 = (int)(time_player1_act/60000.0);
                    time1_des = (int)(time_player1_act/1000.0);
                    time1_des = (time1_des - time1*60)%60;
                    b2.setText("Wite - " + time1 + " min " + time1_des + " sec" );                
                }
                
                if(time_player2_act < 60*1000){
                    time2 = (int)(time_player2_act/1000.0);
                    time2_des = (int)(time_player2_act/100.0);
                    time2_des = (time2_des*10 - time2)%10;
                    b1.setText("Wite - " + time2 + "." + time2_des + " sec" );
                }else{
                    time2 = (int)(time_player2_act/60000.0);
                    time2_des = (int)(time_player2_act/1000.0);
                    time2_des = (time2_des - time2*60)%60;
                    b1.setText("Wite - " + time2 + " min" + time2_des + " sec" );                
                }
                
                
                
                frame_gui.game_desk_menu.time1.repaint();
                frame_gui.game_desk_menu.time1.revalidate();
                frame_gui.game_desk_menu.time2.repaint();
                frame_gui.game_desk_menu.time2.revalidate();
                if(end_time < time_player1_act){
                    thread_active = false;
                    JFrame dialog = new GUI_end_frame(frame_gui, false);
                    
                }
                if(end_time < time_player2_act){
                    thread_active = false;
                    JFrame dialog = new GUI_end_frame(frame_gui, true);
                }
                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    LOGGER.log(Level.SEVERE, "class TIME connot sleep in the run method - {0}", this.toString());
                }
            }
            


        }    
    }
    
    
    class TIME_Thread extends Thread{
    
        
        @Override
        public void run(){
            Date date = new Date();
            Date date_end = new Date();
            

            while(thread_active){
                date_end = new Date();
                
                time_actual =  date_end.getTime() - date.getTime();
                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TIME.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }  
}
