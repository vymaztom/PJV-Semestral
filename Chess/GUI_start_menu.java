
package chess;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * GUI class for creating, saving, loading, choosing game  
 * @author vymaztom
 */
class GUI_start_menu extends JFrame{
    
    Active act;
    
    /*
    start menu values
    */
    JButton[] menu_start_button = new JButton[6];
    
    
    /*
    player1 and player2 values
    */
    ButtonGroup buttonGroup1;
    ButtonGroup buttonGroup2;
    JButton jButton1;
    JButton jButton2;
    JLabel jLabel1;
    JLabel jLabel2;
    JLabel jLabel3;
    JCheckBox jCheckBox1;
    JRadioButton jRadioButton1;
    JRadioButton jRadioButton2;
    JRadioButton jRadioButton3;
    JRadioButton jRadioButton4;
    JSeparator jSeparator1;
    JSlider jSlider1;
    GroupLayout layout;    
    /*
    load
    */
    JFileChooser jFileChooser1;
    /*
    web
    */
    JTextField jTextField1;
    JTextField jTextField2;
    JTextField jTextField3;
    JTextField jTextField4;
    JButton jButton3;
    JButton jButton4;    
    JLabel jLabel4;
    /*
    variables for this class
    */
    int data_from_GUI_menu_type_of_game = 0;
    boolean data_from_GUI_menu_color = true;
    int data_from_GUI_menu_time = 0;
    String data_from_GUI_menu_file = "";
    String data_from_GUI_menu_ip = "";     
    /*
    variables for desk panel
    */
    squer_desk[][] desk_button = new squer_desk[8][8];
    String[] desk_abc = {" ","A", "B", "C", "D", "E", "F","G","H"};
    String[] desk_cisla = {" ","8","7","6","5","4","3","2","1" };
    
    
        
    ImageIcon D_B = new ImageIcon("D_B.png"); 
    ImageIcon H_B = new ImageIcon("H_B.png"); 
    ImageIcon K_B = new ImageIcon("K_B.png"); 
    ImageIcon P_B = new ImageIcon("P_B.png"); 
    ImageIcon S_B = new ImageIcon("S_B.png"); 
    ImageIcon V_B = new ImageIcon("V_B.png"); 
        
    ImageIcon D_W = new ImageIcon("D_W.png"); 
    ImageIcon H_W = new ImageIcon("H_W.png");
    ImageIcon K_W = new ImageIcon("K_W.png");
    ImageIcon P_W = new ImageIcon("P_W.png");
    ImageIcon S_W = new ImageIcon("S_W.png");
    ImageIcon V_W = new ImageIcon("V_W.png");
        
    ImageIcon nulll = new ImageIcon("null.png");    
    
    int[][] desk_int;
    int move_ID = 0;
    int move_x;
    int move_y;
    int last_move_ID = 16;
    int last_move_x;
    int last_move_y; 
    boolean set_move_switch = true;
    
    boolean move_switch = false; 
    boolean move_active = true;
    int move_iter = 0;
    
    /*
    constructors of other classes
    */
    game_menu game_desk_menu;
    GAME game;
    private final Logger LOGGER;
    
    /**
     * methode GUI_start_menu, on the start create menu where you can choos with
     * game you want, this frame still removs componets a add
     * new dueto with button has been used
     * 
     * @param frame_text - is a text whith will be see in a headline a frame
     * @param act - is a constrkrot of data class
     * @param game - is a construktor of game logic
     * @param LOGGER - is a Logger for this apt
     */
    GUI_start_menu(String frame_text,Active act, GAME game, Logger LOGGER){
        super(frame_text);
        this.act = act;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(500, 500));
        this.pack();
        this.setVisible(true);
        this.game = game;
        this.LOGGER = LOGGER;
        act.GUI_start_menu_isActive = false;
    }
    /*
    reloading a menu comolnets
    */
    void reLoad(){
        this.repaint();
        this.revalidate();
    }
    /*
    functino with add componets for the menu
    */
    void menu_start(){
            
            for (int i = 0; i < 6; i++) {
                menu_start_button[i] = new JButton("Press " + i);
            }
  
            menu_start_button[0].addActionListener(new GUI_start_menu.player1(this));
            menu_start_button[5].addActionListener(new GUI_start_menu.player2(this));
            menu_start_button[2].addActionListener(new GUI_start_menu.web(this));
            menu_start_button[3].addActionListener(new GUI_start_menu.end(this));
            menu_start_button[4].addActionListener(new GUI_start_menu.set_game(this));
            menu_start_button[1].addActionListener(new GUI_start_menu.load(this));
            
            menu_start_button[0].setText("1 Player");
            menu_start_button[1].setText("Load game");
            menu_start_button[2].setText("Web game");
            menu_start_button[3].setText("EXIT");
            menu_start_button[4].setText("set desk");
            menu_start_button[5].setText("2 Player");    
            
            
            layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(52, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(menu_start_button[0], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[1], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[2], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(47, 47, 47)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(menu_start_button[3], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[4], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[5], javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(37, 37, 37))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(56, 56, 56)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(menu_start_button[0], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[5], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(57, 57, 57)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(menu_start_button[4], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[2], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(57, 57, 57)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(menu_start_button[1], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(menu_start_button[3], javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(126, Short.MAX_VALUE))
            );

            pack();
    }
    /*
    create choose for one player
    */
    void menu_player1(){
        
        buttonGroup1 = new ButtonGroup();
        buttonGroup2 = new ButtonGroup();
        jSlider1 = new JSlider();
        jButton1 = new JButton();
        jButton2 = new JButton();
        jRadioButton1 = new JRadioButton();
        jRadioButton2 = new JRadioButton();
        jRadioButton3 = new JRadioButton();
        jRadioButton4 = new JRadioButton();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jSeparator1 = new JSeparator();
        jLabel3 = new JLabel();
        jCheckBox1 = new JCheckBox();
        
            
        jButton1.setText("Play");
        jButton2.setText("Back");
        //jButton1.addActionListener(new GUI_menu.ButtonListener_play());
        jButton2.addActionListener(new GUI_start_menu.back_from_player1(this));
        jButton1.addActionListener(new GUI_start_menu.player1_start(this));    
            
        
        

        
        
        
        
        jLabel1.setText("Color");
        jRadioButton3.setText("White");
        jRadioButton4.setText("Black");
        buttonGroup2.add(jRadioButton3);
        buttonGroup2.add(jRadioButton4);
        jRadioButton3.setSelected(true);
        
        
        
        jLabel3.setText("set time to " + jSlider1.getValue() + " min");           
        jCheckBox1.setText("play with out time");
        
        jSlider1.addChangeListener(new GUI_start_menu.play1_time_update(this));
        
        jCheckBox1.addActionListener(new GUI_start_menu.play1_time_swicht(this));
        //jCheckBox1.setVisible(false);            
            
        layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(58, 58, 58))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jRadioButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jRadioButton4)
                        .addGap(80, 80, 80))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jCheckBox1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(255, 255, 255)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(187, 187, 187)
                        .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(262, 262, 262)
                        .addComponent(jLabel3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jLabel1)
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton3)
                    .addComponent(jRadioButton4))
                .addGap(48, 48, 48)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jCheckBox1)
                .addGap(12, 12, 12)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(52, 52, 52))
        );

        pack();   
    
    
    
    
    }
    /*
    create choose for two players
    */    
    void menu_player2(){
        jSlider1 = new JSlider();
        jButton1 = new JButton();
        jButton2 = new JButton();
        jLabel3 = new JLabel();
        jCheckBox1 = new JCheckBox();
        
            
        jButton1.setText("Play");
        jButton2.setText("Back");
        
        jButton2.addActionListener(new GUI_start_menu.back_from_player2(this));
        jButton1.addActionListener(new GUI_start_menu.player2_start(this));    
            
        
        jLabel3.setText("set time to " + jSlider1.getValue() + " min");           
        jCheckBox1.setText("play with out time");
        
        jSlider1.addChangeListener(new GUI_start_menu.play2_time_update(this));
        
        jCheckBox1.addActionListener(new GUI_start_menu.play2_time_swicht(this));
        //jCheckBox1.setVisible(false);            
            
        layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(135, 135, 135)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 159, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(50, 50, 50))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jCheckBox1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(jLabel3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jCheckBox1)
                .addGap(30, 30, 30)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        pack();    
    }
    /*
    create choose for loading game
    */
    void menu_load() throws IOException{
        
        jFileChooser1 = new JFileChooser();
        jFileChooser1.addActionListener(new back_from_player2(this));

        int status = jFileChooser1.showOpenDialog(null);
        if (status == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jFileChooser1.getSelectedFile();
            act.data_from_GUI_menu_type_of_game = 2;
            game.load_game(selectedFile.getAbsolutePath());
            this.getContentPane().removeAll();
            game_desk_menu = new game_menu(this);
            this.setJMenuBar(game_desk_menu);           
            game_desk(true);
            this.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            this.pack();
        }else if(status == JFileChooser.CANCEL_OPTION) {
            System.out.println("calceled");
        }
    }
    /*
    create choose for web game
    */    
    void menu_web(){
        buttonGroup1 = new ButtonGroup();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jTextField1 = new JTextField();
        jTextField2 = new JTextField();
        jTextField3 = new JTextField();
        jTextField4 = new JTextField();
        jLabel4 = new JLabel();
        jLabel2 = new JLabel();
        jRadioButton1 = new JRadioButton();
        jRadioButton2 = new JRadioButton();

        jButton3.setText("start");
        jButton4.setText("back");

        jButton4.addActionListener(new GUI_start_menu.back_from_web(this));
        jButton3.addActionListener(new GUI_start_menu.web_start(this));

        jLabel2.setText("color");
        jLabel4.setText("Oposite IP adress");

        jTextField1.setText("192");
        jTextField2.setText("168");
        jTextField3.setText("1");
        jTextField4.setText("10");

        
        


        jRadioButton1.setText("White");
        jRadioButton2.setText("Black");
        buttonGroup1.add(jRadioButton1);
        buttonGroup1.add(jRadioButton2);
        jRadioButton1.setSelected(true);


        




        layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jRadioButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jRadioButton2)))
                .addGap(50, 50, 50))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(140, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addGap(30, 30, 30)
                .addComponent(jLabel4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton3)
                            .addComponent(jButton4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        pack();
    }
    
    /*
    create a game desk 
    isWhite is a switch for diferens wiev of black a white player
    */
    void game_desk(boolean isWhite){
        setSize(850, 850);
        setMinimumSize(new Dimension(850, 850));
        setLayout(new GridLayout(9, 9));
        if(isWhite){    
            for(int i = 0 ; i < 9 ; i++){

                for(int j = 0 ; j < 9  ; j++){

                    if((i == 0) && (j == 0)){{add(new JLabel(" " ,SwingConstants.CENTER));}}
                    if((i != 0) && (j == 0)){add(new JLabel(desk_cisla[i] ,SwingConstants.CENTER));}
                    if((i == 0) && (j != 0)){add(new JLabel(desk_abc[j] ,SwingConstants.CENTER));}
                    if((i != 0) && (j != 0)){

                        desk_button[i-1][j-1] = new squer_desk();
                        desk_button[i-1][j-1].addActionListener(new ButtonListener(desk_button[i-1][j-1]));
                        desk_button[i-1][j-1].horizontal = i-1;
                        desk_button[i-1][j-1].vertical = j-1;
                        add(desk_button[i-1][j-1]);
                        if((i+j)%2 == 0){
                            desk_button[i-1][j-1].color = Color.WHITE;
                            desk_button[i-1][j-1].setBackground(Color.WHITE);
                        }else{
                            desk_button[i-1][j-1].color = Color.GRAY;
                            desk_button[i-1][j-1].setBackground(Color.GRAY);
                        }
                    }
                }
            }
        }else{
            for(int i = 8 ; i >= 0 ; i--){

                for(int j = 8 ; j >= 0  ; j--){

                    if((i == 8) && (j == 8)){{add(new JLabel(" " ,SwingConstants.CENTER));}}
                    if((i != 8) && (j == 8)){add(new JLabel(desk_cisla[i+1] ,SwingConstants.CENTER));}
                    if((i == 8) && (j != 8)){add(new JLabel(desk_abc[j+1] ,SwingConstants.CENTER));}
                    if((i != 8) && (j != 8)){

                        desk_button[i][j] = new squer_desk();
                        desk_button[i][j].addActionListener(new ButtonListener(desk_button[i][j]));
                        desk_button[i][j].horizontal = i;
                        desk_button[i][j].vertical = j;
                        add(desk_button[i][j]);
                        if((i+j)%2 == 0){
                            desk_button[i][j].color = Color.WHITE;
                            desk_button[i][j].setBackground(Color.WHITE);
                        }else{
                            desk_button[i][j].color = Color.GRAY;
                            desk_button[i][j].setBackground(Color.GRAY);
                        }
                    }
                }
            }        
        }         
    }
    /*
    reload a componets dueto int representation s game situatio
    */
    void game_reLoad(int[][] desk_int){
        for(int i = 0 ; i < 8 ; i++){
            for(int j = 0 ; j < 8 ; j++){
                if(desk_int[i][j] != -1){
                    if(desk_int[i][j] < 16){
                        if(desk_int[i][j] < 8){
                            desk_button[i][j].setIcon(P_W);
                        }else{
                            if((desk_int[i][j] == 8) || (desk_int[i][j] == 15)){desk_button[i][j].setIcon(V_W);}
                            if((desk_int[i][j] == 9) || (desk_int[i][j] == 14)){desk_button[i][j].setIcon(H_W);}
                            if((desk_int[i][j] == 10) || (desk_int[i][j] == 13)){desk_button[i][j].setIcon(S_W);}
                            if(desk_int[i][j] == 11){desk_button[i][j].setIcon(K_W);}
                            if(desk_int[i][j] == 12){desk_button[i][j].setIcon(D_W);}
                        }
                    }else{
                        if(desk_int[i][j] < 24){
                            desk_button[i][j].setIcon(P_B);
                        }else{
                            if((desk_int[i][j] == 24) || (desk_int[i][j] == 31)){desk_button[i][j].setIcon(V_B);}
                            if((desk_int[i][j] == 25) || (desk_int[i][j] == 30)){desk_button[i][j].setIcon(H_B);}
                            if((desk_int[i][j] == 26) || (desk_int[i][j] == 29)){desk_button[i][j].setIcon(S_B);}
                            if(desk_int[i][j] == 27){desk_button[i][j].setIcon(K_B);}
                            if(desk_int[i][j] == 28){desk_button[i][j].setIcon(D_B);}                            
                        }                        
                    }
                }else{
                    desk_button[i][j].setIcon(nulll);
                }
            }
        }    
    }
        
    
    /*
    ------------------------other classes----------------------------
    */
    
    
    /*
    special button class for gam desk
    */
    class squer_desk extends JButton{
        int vertical;
        int horizontal;
        Color color;
    }    
    
    /*
    action listener for game desk to do moves
    */
    class ButtonListener implements ActionListener {
        
        squer_desk but;
        
            
        ButtonListener(squer_desk but){
            this.but = but;
                
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if(!act.GUI_start_menu_isActive){    
                if(!move_switch){     
                    if(move_active){
                        desk_int = game.game.desk.getINTdesk();

                        if(desk_int[this.but.horizontal][this.but.vertical] != -1 ){
                            move_ID = desk_int[this.but.horizontal][this.but.vertical];
                            //System.out.printf("Button %d %d \n", this.but.horizontal,this.but.vertical);
                            desk_button[this.but.horizontal][this.but.vertical].setBackground(Color.BLUE);
                            last_move_x = this.but.horizontal;
                            last_move_y = this.but.vertical;
                            move_active = false;
                        }
                    }else{

                        move_x = this.but.horizontal;
                        move_y = this.but.vertical;
                        //System.out.printf("Button move %d %d \n", this.but.horizontal,this.but.vertical);

                        if(game.game.move(move_ID, move_x, move_y, 0)){

                            game_reLoad(game.game.desk.getINTdesk());
                            System.out.println("OK");
                        }else{
                            System.out.println("bead move");
                        }
                        move_active = true;
                        desk_button[last_move_x][last_move_y].setBackground(desk_button[last_move_x][last_move_y].color);


                    }
                }else{
                    //System.out.printf("move ID %d to %d %d \n", move_ID,move_x,move_y);
                }
            }else{
                //System.out.println("ahoj");
                    if(set_move_switch){
                        desk_int = game.game.desk.getINTdesk();

                        if(desk_int[this.but.horizontal][this.but.vertical] != -1 ){
                            move_ID = desk_int[this.but.horizontal][this.but.vertical];
                            
                            desk_button[this.but.horizontal][this.but.vertical].setBackground(Color.BLUE);
                            last_move_x = this.but.horizontal;
                            last_move_y = this.but.vertical;
                            set_move_switch = false;
                        }
                    }else{

                        move_x = this.but.horizontal;
                        move_y = this.but.vertical;
                        if((last_move_x == move_x) && (last_move_y == move_y)){
                            if(game.game.move(move_ID, move_x, move_y,1)){
                                game_reLoad(game.game.desk.getINTdesk());
                                System.out.println("OK");
                            }else{
                                System.out.println("bead move");
                            }
                        }else if(desk_int[this.but.horizontal][this.but.vertical] == -1 ){
                            if(game.game.move(move_ID, move_x, move_y,2)){
                                game_reLoad(game.game.desk.getINTdesk());
                                System.out.println("OK");
                            }else{
                                System.out.println("bead move");
                            }                            
                        }
                        set_move_switch = true;
                        desk_button[last_move_x][last_move_y].setBackground(desk_button[last_move_x][last_move_y].color);
                        

                    }            
            
            } 
        }
    }    
    /*
    menu for game desk where you can go back, save game, start game atd.. 
    */
    class game_menu extends JMenuBar{
        JButton game;   
        JButton  start;
        JButton save;
        JButton exit;
        GUI_start_menu me;
        JButton time1;
        JButton time2;
        
        
        game_menu(GUI_start_menu me){
            this.me = me;
            //game = new JButton("GAME");    
            start = new JButton("START");
            save = new JButton("SAVE");
            exit = new JButton("EXIT");
            time1 = new JButton("White Player");
            time2 = new JButton("Balck Player");
            
            
            exit.addActionListener(new menu_exit(me));
            save.addActionListener(new menu_save(me));
            start.addActionListener(new menu_start(me));
            
            //add(game);
            //add(about);
            if((act.data_from_GUI_menu_time_acive) && (act.data_from_GUI_menu_type_of_game==2)){
              add(save);      
            }
            if(act.GUI_start_menu_isActive){
              add(start);
            }
            
            add(exit);
            
            add(time1);
            add(time2);
            
            
            //add(new JButton("time P1"));
            
            //add(new JButton("time P2"));
        }
    }
    
    /*****************actions******************/
    
    /*
    action listener for exit game
    */
    class end implements ActionListener {
        
        GUI_start_menu me;
        
        end(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }    
    /*
    action listener creting game of one player
    */    
    class player1 implements ActionListener {
        
        GUI_start_menu me;
        
        player1(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < 6; i++) {
                me.remove(menu_start_button[i]);
            }
            me.menu_player1();
            reLoad();
            me.pack();
        }
    } 
    class back_from_player1 implements ActionListener {
        
        GUI_start_menu me;
        
        back_from_player1(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {

            me.remove(jButton1);
            me.remove(jButton2);
            me.remove(jLabel1);
            me.remove(jLabel2);
            me.remove(jLabel3);
            me.remove(jCheckBox1);
            me.remove(jRadioButton1);
            me.remove(jRadioButton2);
            me.remove(jRadioButton3);
            me.remove(jRadioButton4);
            me.remove(jSeparator1);
            me.remove(jSlider1);
            
            me.menu_start();
            me.pack();
        }
    } 
    class play1_time_swicht implements ActionListener {
        
        GUI_start_menu me;
        
        play1_time_swicht(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(jCheckBox1.isSelected()){
                jSlider1.setVisible(false);
                jLabel3.setVisible(false);
            }else{
                jSlider1.setVisible(true);
                jLabel3.setVisible(true);
            }
            
            
            me.reLoad();
            me.pack();
        }
    } 
    class play1_time_update implements ChangeListener {
        
        GUI_start_menu me;
        
        play1_time_update(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void stateChanged(ChangeEvent e) {
            jLabel3.setText("set time to " + jSlider1.getValue() + " min");
            me.reLoad();
            me.pack();
        }
    }
    class player1_start implements ActionListener {
        
        GUI_start_menu me;
        
        player1_start(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            
            act.data_from_GUI_menu_color = jRadioButton3.isSelected();
            //act.data_from_GUI_menu_file = me.data_from_GUI_menu_file;
            //act.data_from_GUI_menu_ip = me.data_from_GUI_menu_ip;
            act.data_from_GUI_menu_time_acive = jCheckBox1.isSelected();
            if(!jCheckBox1.isSelected()){
                act.data_from_GUI_menu_time = jSlider1.getValue();
            }
            act.data_from_GUI_menu_type_of_game = 1;
            try {
                game.create_game();
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            me.getContentPane().removeAll();
            game_desk_menu = new game_menu(me);
            me.setJMenuBar(game_desk_menu);
            game_desk(act.data_from_GUI_menu_color);
            me.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            me.pack();
            if(!act.data_from_GUI_menu_time_acive){
                game.game.start_time(act.data_from_GUI_menu_time*60*1000,0,0);
                game.start_time();
            }
        }
    }    
    /*
    action listener creting game of two players
    */ 
    class player2 implements ActionListener {
        
        GUI_start_menu me;
        
        player2(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < 6; i++) {
                me.remove(menu_start_button[i]);
            }
            me.menu_player2();
            reLoad();
            me.pack();
        }
    } 
    class back_from_player2 implements ActionListener {
        
        GUI_start_menu me;
        
        back_from_player2(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {

            me.getContentPane().removeAll();
            
            me.menu_start();
            me.pack();
        }
    } 
    class play2_time_swicht implements ActionListener {
        
        GUI_start_menu me;
        
        play2_time_swicht(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(jCheckBox1.isSelected()){
                jSlider1.setVisible(false);
                jLabel3.setVisible(false);
            }else{
                jSlider1.setVisible(true);
                jLabel3.setVisible(true);
            }
            
            
            me.reLoad();
            me.pack();
        }
    } 
    class play2_time_update implements ChangeListener {
        
        GUI_start_menu me;
        
        play2_time_update(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void stateChanged(ChangeEvent e) {
            jLabel3.setText("set time to " + jSlider1.getValue() + " min");
            me.reLoad();
            me.pack();
        }
    }
    class player2_start implements ActionListener {
        
        GUI_start_menu me;
        
        player2_start(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            
            act.data_from_GUI_menu_color = true;
            //act.data_from_GUI_menu_file = me.data_from_GUI_menu_file;
            //act.data_from_GUI_menu_ip = me.data_from_GUI_menu_ip;
            act.data_from_GUI_menu_time_acive = jCheckBox1.isSelected();
            if(!jCheckBox1.isSelected()){
                
                act.data_from_GUI_menu_time = jSlider1.getValue();
            }
            act.data_from_GUI_menu_type_of_game = 2;
            try {
                game.create_game();
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            me.getContentPane().removeAll();
            game_desk_menu = new game_menu(me);
            me.setJMenuBar(game_desk_menu);           
            game_desk(act.data_from_GUI_menu_color);
            me.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            me.pack();
            if(!act.data_from_GUI_menu_time_acive){
                game.game.start_time(act.data_from_GUI_menu_time*60*1000,0,0);
                game.start_time();
            }            
        }
    }   
    /*
    action listener creting web game
    */     
    class web implements ActionListener {
        
        GUI_start_menu me;
        
        web(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < 6; i++) {
                me.remove(menu_start_button[i]);
            }
            me.menu_web();
            //me.setSize(new Dimension(363, 163));
            reLoad();
            me.pack();
        }
    } 
    class back_from_web implements ActionListener {
        
        GUI_start_menu me;
        
        back_from_web(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {

            me.getContentPane().removeAll();

            
            me.menu_start();
            me.pack();
        }
    } 
    class web_start implements ActionListener {
        
        GUI_start_menu me;
        
        web_start(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            
            act.data_from_GUI_menu_color = jRadioButton1.isSelected();
            act.data_from_GUI_menu_ip = jTextField2.getText() + "." + jTextField1.getText() + "." + jTextField3.getText() + "." + jTextField4.getText();
            act.data_from_GUI_menu_type_of_game = 3;
            try {
                game.create_game();
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            me.getContentPane().removeAll();
            game_desk_menu = new game_menu(me);
            me.setJMenuBar(game_desk_menu);        
            game_desk(act.data_from_GUI_menu_color);
            me.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            me.pack();
        }
    }
    /*
    action listener for loading a game
    */     
    class load implements ActionListener {
        
        GUI_start_menu me;
        
        load(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < 6; i++) {
                me.remove(menu_start_button[i]);
            }
            try {
                me.menu_load();
                act.data_from_GUI_menu_time_acive = true;
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            //me.setSize(new Dimension(363, 163));
            reLoad();
            me.pack(); 
        }
    }
    class back_from_load implements ActionListener {
        
        GUI_start_menu me;
        
        back_from_load(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {

            me.remove(jFileChooser1);

            
            me.menu_start();
            me.pack();
        }
    }
    class load_start implements ActionListener {
        
        GUI_start_menu me;
        
        load_start(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            
            //act.data_from_GUI_menu_color = jRadioButton3.isSelected();
            //act.data_from_GUI_menu_file = me.data_from_GUI_menu_file;
            //act.data_from_GUI_menu_ip = me.data_from_GUI_menu_ip;
            /*
            act.data_from_GUI_menu_time_acive = jCheckBox1.isSelected();
            if(jCheckBox1.isSelected()){
                act.data_from_GUI_menu_time = jSlider1.getValue();
            }
            */
            act.data_from_GUI_menu_type_of_game = 4;
            try {
                game.create_game();
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            me.remove(jButton1);
            me.remove(jButton2);
            me.remove(jLabel1);
            me.remove(jLabel2);
            me.remove(jLabel3);
            me.remove(jCheckBox1);
            me.remove(jRadioButton1);
            me.remove(jRadioButton2);
            me.remove(jRadioButton3);
            me.remove(jRadioButton4);
            me.remove(jSeparator1);
            me.remove(jSlider1);
            game_desk(act.data_from_GUI_menu_color);
            me.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            me.pack();
        }
    }    
    /*
    action listener for desk_menu to go back to mine menu
    */     
    class menu_exit implements ActionListener {
        
        GUI_start_menu me;
        
        menu_exit(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            me.getContentPane().removeAll();
            me.setSize(500, 500);
            me.setMinimumSize(new Dimension(0, 0));
            me.setJMenuBar(null);
            me.menu_start();
            me.reLoad();
            me.pack();
        }
    }
    /*
    action listener for desk_menu to save game
    */    
    class menu_save implements ActionListener {
        
        GUI_start_menu me;
        
        menu_save(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            jFileChooser1 = new JFileChooser();
            int status = jFileChooser1.showOpenDialog(null);
            if (status == JFileChooser.APPROVE_OPTION) {
                try {
                    File selectedFile = jFileChooser1.getSelectedFile();
                    //System.out.println(selectedFile.getParent());
                    //System.out.println(selectedFile.getName());
                    //System.out.println(selectedFile.getAbsolutePath());
                    game.save_game(selectedFile.getAbsolutePath());
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, "problem dueto saving game in - {0}", this.toString());
                }
            }else if(status == JFileChooser.CANCEL_OPTION) {
                //System.out.println("calceled");
            }           
            
            
            me.reLoad();
            me.pack();
        }
    }        
    /*
    action listener for desk_menu to start game in set game type game
    */
    class menu_start implements ActionListener {
        
        GUI_start_menu me;
        
        menu_start(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            act.GUI_start_menu_isActive = false;
            me.game_desk_menu.remove(me.game_desk_menu.start);
            
            
            me.reLoad();
            me.pack();
        }
    }        
    /*
    action listener for type game set desk situation
    */    
    class set_game implements ActionListener {
        
        GUI_start_menu me;
        
        set_game(GUI_start_menu me){
            this.me = me;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            
            act.data_from_GUI_menu_color = true;
            act.GUI_start_menu_isActive = true;
            act.data_from_GUI_menu_type_of_game = 5;
            try {
                game.create_game();
            } catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "problem dueto creating game in - {0}", this.toString());
            }
            me.getContentPane().removeAll();
            game_desk_menu = new game_menu(me);
            me.setJMenuBar(game_desk_menu);           
            game_desk(act.data_from_GUI_menu_color);
            me.game_reLoad(game.game.desk.getINTdesk());
            reLoad();
            me.pack();            
        }
    }    
}
    

