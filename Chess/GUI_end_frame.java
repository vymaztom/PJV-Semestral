package chess;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * frame for informatin of end of game
 * @author vymaztom
 */
class GUI_end_frame extends JFrame{
    
    JButton jButton3;
    JLabel jLabel2;
    GUI_start_menu GUI_menu;
    /*
    constructor ceate a frame, if you click on the button into this frame this thread will be end
    */
    GUI_end_frame(GUI_start_menu GUI_menu, boolean whiteIsWinner){
        this.GUI_menu = GUI_menu;
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        
        jButton3 = new JButton();
        jLabel2 = new JLabel();
        
        jButton3.setText("go to menu");
        jButton3.addActionListener(new end(this));
        if(whiteIsWinner){
            jLabel2.setText("White player win, congratulation!! ");
        }else{
            jLabel2.setText("Black player win, congratulation!! ");
        }
    
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 79, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(149, 149, 149)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addContainerGap(51, Short.MAX_VALUE))
        );

        pack();
        this.setVisible(true);    
    }
    
    /*
        action for end of frame a goto maine menu
    */
    class end implements ActionListener {
     
        JFrame me;
     
        end(JFrame me){
            this.me = me;
        }
     
        @Override
        public void actionPerformed(ActionEvent e) {    
            GUI_menu.getContentPane().removeAll();
            GUI_menu.setSize(500, 500);
            GUI_menu.setMinimumSize(new Dimension(0, 0));
            GUI_menu.setJMenuBar(null);
            GUI_menu.menu_start();
            GUI_menu.reLoad();
            GUI_menu.pack();
            me.dispose();
        }
    }   
}
