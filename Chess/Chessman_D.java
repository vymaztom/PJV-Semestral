
package chess;

/**
 * figure queen
 * javadoc is same as chessman
 * @author vymaztom
 */
class Chessman_D extends Chessman {

    Chessman_D(int ID, int position_x, int position_y) {
        super(ID, position_x, position_y);
    }
    
    @Override
    boolean move(Chessman[] oposit, int[][] desk, int x, int y){
        
        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
 
        
        if((delta_x == 0) && (delta_y == 0)){
            return false;
        }        
        
        if((delta_x == 0) && (delta_y > 0)){
                for(int i = this.position_y+1 ; i < y; i++){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }            
        }
        
        if((delta_x == 0) && (delta_y < 0)){
                for(int i = this.position_y-1 ; i > y; i--){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }          
        }
        
        if((delta_x > 0) && (delta_y == 0)){
                for(int i = this.position_x+1 ; i < x; i++){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }        
        }
        
        if((delta_x < 0) && (delta_y == 0)){
               for(int i = this.position_x-1 ; i > x; i--){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }        
        }
        
        if((delta_x < 0) && (delta_y < 0)){
                if(delta_x == delta_y){
                    
                    for(int i = 1 ; i < (-1)*delta_x; i++){
                        if(desk[this.position_x-i][this.position_y-i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                oposit[che - 16].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                oposit[che].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        this.set_position(x, y);
                        return true;
                    }
                }        
        }
        
        if((delta_x < 0) && (delta_y > 0)){
                if(delta_x*(-1) == delta_y){
                    
                    for(int i = 1 ; i < delta_y; i++){
                        if(desk[this.position_x-i][this.position_y+i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                oposit[che - 16].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                oposit[che].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        this.set_position(x, y);
                        return true;
                    }
                }
                return false;        
        }
        
        if((delta_x > 0) && (delta_y < 0)){
                if(delta_x == (-1)*delta_y){
                    
                    for(int i = 1 ; i < delta_x; i++){
                        if(desk[this.position_x+i][this.position_y-i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                oposit[che - 16].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                oposit[che].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        this.set_position(x, y);
                        return true;
                    }
                }
                return false;        
        }
        
        if((delta_x > 0) && (delta_y > 0)){
                if(delta_x == delta_y){
                    
                    for(int i = 1 ; i < delta_x; i++){
                        if(desk[this.position_x+i][this.position_y+i] != -1){
                            return false;
                        }
                    }
                    if(desk[this.position_x+delta_x][this.position_y+delta_y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                oposit[che - 16].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                oposit[che].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        this.set_position(x, y);
                        return true;
                    }
                    
                }
                return false;        
        }
        
        

        
        return false;
    }
    
    @Override
    boolean posible_move(Chessman[] oposit, int[][] desk, int x, int y){
        
        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
      
        if((delta_x == 0) && (delta_y == 0)){
            return false;
        }        
        
        if((delta_x == 0) && (delta_y > 0)){
                for(int i = this.position_y+1 ; i < y; i++){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            return true;
                        }
                        return false;
                    } 
                        
                }else{

                    return true;
                }            
        }
        
        if((delta_x == 0) && (delta_y < 0)){
                for(int i = this.position_y-1 ; i > y; i--){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    return true;
                }          
        }
        
        if((delta_x > 0) && (delta_y == 0)){
                for(int i = this.position_x+1 ; i < x; i++){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    return true;
                }        
        }
        
        if((delta_x < 0) && (delta_y == 0)){
               for(int i = this.position_x-1 ; i > x; i--){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    return true;
                }        
        }
        
        if((delta_x < 0) && (delta_y < 0)){
                if(delta_x == delta_y){
                    
                    for(int i = 1 ; i < (-1)*delta_x; i++){
                        if(desk[this.position_x-i][this.position_y-i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){

                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){

                                return true;
                            }
                            return false;
                        } 
                        
                    }else{

                        return true;
                    }
                }        
        }
        
        if((delta_x < 0) && (delta_y > 0)){
                if(delta_x*(-1) == delta_y){
                    
                    for(int i = 1 ; i < delta_y; i++){
                        if(desk[this.position_x-i][this.position_y+i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        return true;
                    }
                }
                return false;        
        }
        
        if((delta_x > 0) && (delta_y < 0)){
                if(delta_x == (-1)*delta_y){
                    
                    for(int i = 1 ; i < delta_x; i++){
                        if(desk[this.position_x+i][this.position_y-i] != -1){
                            return false;
                        }
                    }
                    if(desk[x][y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){

                                return true;
                            }
                            return false;
                        } 
                        
                    }else{

                        return true;
                    }
                }
                return false;        
        }
        
        if((delta_x > 0) && (delta_y > 0)){
                if(delta_x == delta_y){
                    
                    for(int i = 1 ; i < delta_x; i++){
                        if(desk[this.position_x+i][this.position_y+i] != -1){
                            return false;
                        }
                    }
                    if(desk[this.position_x+delta_x][this.position_y+delta_y] != -1){
                            
                        int che = desk[x][y];
                
                        if (this.ID < 16){
                            if(che > 15){
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                return true;
                            }
                            return false;
                        } 
                        
                    }else{
                        return true;
                    }
                    
                }
                return false;        
        }
        
        

        
        return false;
    }
    
}