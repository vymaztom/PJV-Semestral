
package chess;

import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
Mine class
*/
public class Chess {
    
    /*
    basic variables for game
    */
    static GAME game;
    static GUI_start_menu GUI_start_menu_frame;
    static Active act;
    /*
    logger for this game
    */
    private static Logger LOGGER = Logger.getLogger("InfoLogging");
    

    public static void main(String[] args) throws InterruptedException{
        act = new Active();
        game = new GAME(act, LOGGER);
        GUI_start_menu_frame = new GUI_start_menu("Chess menu",act,game,LOGGER);
        game.gui_frame = GUI_start_menu_frame;
        GUI_start_menu_frame.menu_start();
    }   
}
