
package chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vymaztom
 */
class com_net_thread extends Thread{
    
    ServerSocket serverSocket = null;
    Socket clientSocket = null;
    
    String inputLine;
    String outputLine;
    
    PrintWriter out;
    BufferedReader in;

    
    
    boolean IsActive = true;
    boolean colorIsWhite;
    GAME.game_type game;    
    GUI_start_menu gui_frame;
    Active act;
    private final Logger LOGGER;
    
    /*
    white player is server
    balck palyer is cliet
    */
    
    /**
     * class for web comuniaction,exactly for connecting a listenig
     * @param act - is a data class
     * @param game - is a constructor of a game_type
     * @param gui_frame - is aconstructor of a gui
     * @param colorIsWhite - boolean vaule with color you are
     * @param LOGGER - logger of this apt
     * @throws IOException 
     * @throws InterruptedException 
     */
    com_net_thread(Active act, GAME.game_type game, GUI_start_menu gui_frame, boolean colorIsWhite, Logger LOGGER) throws IOException, InterruptedException{
        this.act = act;
        this.game = game;
        this.gui_frame = gui_frame;
        this.colorIsWhite = colorIsWhite;
        this.LOGGER = LOGGER;
        if(colorIsWhite){
            try {
                serverSocket = new ServerSocket(4445);
            } catch (IOException e) {
                System.err.println("Could not listen on port: 4445.");
                System.exit(1);
            }


            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                System.err.println("Accept failed.");
                System.exit(1);
            }    
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));            
        }else{
            while(IsActive){
                try {  
                    IsActive = false;
                    //System.out.println(act.data_from_GUI_menu_ip);
                    clientSocket = new Socket(act.data_from_GUI_menu_ip, 4445);
                    out = new PrintWriter(clientSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                } catch (UnknownHostException e) {
                    LOGGER.log(Level.INFO, "web game cannot connect");
                    Thread.sleep(10);
                    IsActive = true;
                } catch (IOException e) {
                    LOGGER.log(Level.INFO, "web game cannot connect");
                    Thread.sleep(10);
                    IsActive = true;
                }
            }
            IsActive = true;            
        }
    }
    

    
    
    
    /*
    listening, and if same data come, method run in a game_type have been call 
     */
    @Override
    public void run(){
        Integer[] data = new Integer[3];
        String[] d = new String[3];
        while(IsActive){
            try {
                while ((inputLine = in.readLine()) != null) {
                    //System.out.println(inputLine);
                    d = inputLine.split("a");
                    for(int i = 0 ; i < 3 ; i++){
                        data[i] = Integer.parseInt(d[i]);
                    }
                    //game.move(data[0], data[1], data[2], 0);
                    if(this.colorIsWhite){
                        if(game.desk.player2.move(data[0], game.desk.player1.figurky, game.desk.desk_string, data[1], data[2])){
                            gui_frame.game_desk_menu.time1.setText("White Player - on move");
                            gui_frame.game_desk_menu.time2.setText("Balck Player");
                            gui_frame.game_reLoad(game.desk.getINTdesk());
                            game.move_iter++;
                        }                    
                    }else{
                        if(game.desk.player1.move(data[0], game.desk.player2.figurky, game.desk.desk_string, data[1], data[2])){
                            gui_frame.game_desk_menu.time1.setText("White Player");
                            gui_frame.game_desk_menu.time2.setText("Balck Player - on move");                            
                            gui_frame.game_reLoad(game.desk.getINTdesk());
                            game.move_iter++;
                        }                    
                    }

                    
                }
            } catch (IOException ex) {
                System.out.println("err");
                LOGGER.log(Level.SEVERE, "problem dueto web comunicatins - {0}", this.toString());
            }
        }        
    }
}
