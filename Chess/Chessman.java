
package chess;

/**
 * Basic class for Chessmans
 * 
 * 
 */
class Chessman {
    /**
     * true if is the first move for this figure
     */
    boolean first_move = true;
    /**
     * true if is this figure active (if is the figure in the game)
     */
    boolean active = true; 
    int position_x = 0; // vertikal
    int position_y = 0; // horizontal
    int ID = 0;
    
    /**
     * 
     * @param ID - specific int value, every chessman have diferent 
     * @param position_x  - vertical location in a desk
     * @param position_y  - horizontal location in a desk
     */
    Chessman(int ID,int position_x, int position_y){
        this.position_x = position_x;
        this.position_y = position_y;
        this.ID = ID;
    }
    /**
     * 
     * @param oposit - list of oposit figures 
     * @param desk - int representation of desk where is IDs of active figures and -1 otherwise 
     * @param x  - vertical coordinates on the board where the figure wants to go
     * @param y  - horizonatal coordinates on the board where the figure wants to go
     * @return function return true if move was successful false otherwise
     */
    boolean move(Chessman[] oposit, int[][] desk, int x, int y){
        this.position_x = x;
        this.position_y = y;
        return true;
    }
    /**
     * 
     * @param oposit - list of oposit figures 
     * @param desk - int representation of desk where is IDs of active figures and -1 otherwise 
     * @param x  - vertical coordinates on the board where the figure wants to go
     * @param y  - horizonatal coordinates on the board where the figure wants to go
     * @return function return true if move is possible false otherwise
     */
    boolean posible_move(Chessman[] oposit, int[][] desk, int x, int y){
        return true;
    }
    /**
     * 
     * @param position_x - set vertical coordinates on the board 
     * @param position_y - set horizonatal coordinates on the board 
     */
    void set_position(int position_x, int position_y){
        this.position_x = position_x;
        this.position_y = position_y;
    }
}

