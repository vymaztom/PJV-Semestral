
package chess;

/**
 * figure rook
 * javadoc is same as chessman
 * @author vymaztom
 */
class Chessman_V extends Chessman {

    Chessman_V(int ID, int position_x, int position_y) {
        super(ID, position_x, position_y);
    }
    @Override
    boolean move(Chessman[] oposit, int[][] desk, int x, int y){
        
        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
        
       
        
        
        if(delta_x == 0){
            if(delta_y > 0){
                
                for(int i = this.position_y+1 ; i < y; i++){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }                
                
            }
            
            if(delta_y < 0){
                
                for(int i = this.position_y-1 ; i > y; i--){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }                
                
            }            
            
            
            return false;        
        }
        
        if(delta_y == 0){
            if(delta_x > 0){
                for(int i = this.position_x+1 ; i < x; i++){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }            
            }
            if(delta_x < 0){
                for(int i = this.position_x-1 ; i > x; i--){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){
                            oposit[che - 16].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){
                            oposit[che].active = false;
                            this.set_position(x, y);
                            return true;
                        }
                        return false;
                    } 
                        
                }else{
                    this.set_position(x, y);
                    return true;
                }            
            }
            return false;        
        }
        return false;
    }
    
    @Override
    boolean posible_move(Chessman[] oposit, int[][] desk, int x, int y){
        
        int delta_x = x - this.position_x;
        int delta_y = y - this.position_y;
        
       
        
        
        if(delta_x == 0){
            if(delta_y > 0){
                
                for(int i = this.position_y+1 ; i < y; i++){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){

                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){

                            return true;
                        }
                        return false;
                    } 
                        
                }else{

                    return true;
                }                
                
            }
            
            if(delta_y < 0){
                
                for(int i = this.position_y-1 ; i > y; i--){
                    
                    if(desk[this.position_x][i] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){

                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){

                            return true;
                        }
                        return false;
                    } 
                        
                }else{

                    return true;
                }                
                
            }            
            
            
            return false;        
        }
        
        if(delta_y == 0){
            if(delta_x > 0){
                for(int i = this.position_x+1 ; i < x; i++){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){

                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){

                            return true;
                        }
                        return false;
                    } 
                        
                }else{

                    return true;
                }            
            }
            if(delta_x < 0){
                for(int i = this.position_x-1 ; i > x; i--){
                    
                    if(desk[i][this.position_y] != -1){
                        return false;
                    }
                }
                if(desk[x][y] != -1){
                            
                    int che = desk[x][y];
                
                    if (this.ID < 16){
                        if(che > 15){

                            return true;
                        }
                        return false;
                    }else{
                        if(che < 16){

                            return true;
                        }
                        return false;
                    } 
                        
                }else{

                    return true;
                }            
            }
            return false;        
        }
        return false;
    }
    

}