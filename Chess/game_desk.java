
package chess;


import java.util.ArrayList;
import java.util.Arrays;


/**
 * Pulic class game_desk simulate game rules. It is type of state machine.
 * @author vymaztom
 */

public class game_desk {
    
    /*
    representation of two playres;
    */
    player_White player1 = new player_White();
    player_Black player2 = new player_Black();
    /*
    int representation of game situation
    */
    int[][] desk_string = new int[8][8];
    
    /*
    save new representatin in to deskt_string
    */
    void ToInt(){
        for(int i = 0 ; i < 8 ; i++){
            for(int j = 0 ; j < 8 ; j++){
                this.desk_string[i][j] = -1;
            }
        }
        for(int i = 0 ; i < 16 ; i++){
            if(player1.figurky[i].active){
                this.desk_string[player1.figurky[i].position_x][player1.figurky[i].position_y] = player1.figurky[i].ID;
            }
        }
        for(int i = 0 ; i < 16 ; i++){
            if(player2.figurky[i].active){
                this.desk_string[player2.figurky[i].position_x][player2.figurky[i].position_y] = player2.figurky[i].ID;
            }
        }
        /*
        code for print matrix of game situation
        */
        for(int i = 0 ; i < 8 ; i++){
            for(int j = 0 ; j < 8 ; j++){
                System.out.printf(" %2d ", this.desk_string[i][j]);
            }
            System.out.printf("\n");
        }
        
    }
    /*
    return the actual game rapresention int desk
    */ 
    int[][] getINTdesk(){
        this.ToInt();
        return this.desk_string;
    }
   
    
    /**
     * class for white player - add figure
     */
    class player_White{
    
        Chessman[] figurky = new Chessman[16];
        int IDnotifikation = 0;
        
        
        player_White(){

            for(int i = 0 ; i < 8 ; i++){
                this.figurky[i] = new Chessman_U_W(i,6,i);
            }
            
            this.figurky[8] = new Chessman_V(8,7,0);
            this.figurky[9] = new Chessman_J(9,7,1);
            this.figurky[10] = new Chessman_S(10,7,2);
            this.figurky[11] = new Chessman_D(11,7,3);
            
            this.figurky[12] = new Chessman_K(12,7,4);
            this.figurky[13] = new Chessman_S(13,7,5);
            this.figurky[14] = new Chessman_J(14,7,6);
            this.figurky[15] = new Chessman_V(15,7,7);
      
        }
        
        boolean move(int ID_figure, Chessman[] oposit,int[][] desk_int, int x, int y){
            return this.figurky[ID_figure].move(oposit, desk_int, x, y);
        }
        
        
    }
    /**
     * class for black player - add figure
     */    
    class player_Black{
    
        Chessman[] figurky = new Chessman[16];
        int IDnotifikation = 0;
        
        
        player_Black(){
            
            
            
            for(int i = 0 ; i < 8 ; i++){
                figurky[i] = new Chessman_U_B(i+16,1,i);
            }
            
            figurky[8] = new Chessman_V(24,0,0);
            figurky[9] = new Chessman_J(25,0,1);
            figurky[10] = new Chessman_S(26,0,2);
            figurky[11] = new Chessman_D(27,0,3);
            
            figurky[12] = new Chessman_K(28,0,4);
            figurky[13] = new Chessman_S(29,0,5);
            figurky[14] = new Chessman_J(30,0,6);
            figurky[15] = new Chessman_V(31,0,7);
      
        }
        
        boolean move(int ID_figure, Chessman[] oposit,int[][] desk_int, int x, int y){
            return this.figurky[ID_figure-16].move(oposit, desk_int, x, y);
        }
        
        
    }
}
