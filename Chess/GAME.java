
package chess;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;
import javax.swing.JFrame;
/**
 * class GAME is a easy switch for creating and calcule game
 * @author vymaztom
 */

    class GAME{
        
        game_type game;
        Active act;
        GUI_start_menu gui_frame;
        com_net_thread net;
        
        int[][] posible_moves = new int[8][8];
        int[][] int_desk =  new int[8][8];
                
        boolean player_switch = true;
        private final Logger LOGGER;
        
        /**
         * 
         * @param act - class where are save data from gui 
         */
        GAME(Active act, Logger LOGGER){
            this.LOGGER = LOGGER;
            this.act = act;
        }
        
        /**
         * 
         * @param me - list of figures of AI player
         * @param oposit  - list of figures of person
         * 
         *  function do firs move of first active figure from list
         */
        void AI_move(Chessman[] me , Chessman[] oposit){
            int_desk = game.desk.getINTdesk();
            for(int i = 0 ; i < 16 ; i++){
                if(me[i].active){
                    for(int j = 0 ; j < 8 ; j++){
                        for(int k = 0 ; k < 8 ; k++){
                            if(me[i].move(oposit, int_desk, j, k)){
                                return;
                            }
                        }
                    }
                }
            }
        }
        
        
        /**
         * 
         * @param me - list of figures of player whitch have had played
         * @param oposit -list of figures of player whitch will play 
         * @return function return true if next player cannot play, false otherwise
         */
        boolean isEnd_player(Chessman[] me , Chessman[] oposit){
            
            posible_moves = new int[8][8];
            int_desk = game.desk.getINTdesk();
            
            for(int i = 0 ; i < 8 ; i++ ){
                for(int j = 0 ; j < 8 ; j++){
                    posible_moves[i][j] = -1;
                }
            }
            
            if(me[12].active){
                for(int i = 0 ; i < 16 ; i++){
                    for(int k = 0 ; k < 8 ; k++ ){
                        for(int j = 0 ; j < 8 ; j++){
                            if(oposit[i].posible_move(oposit, int_desk, k, j)){
                               posible_moves[k][j] = 1; 
                            }
                        }
                    }                    
                }
                if(posible_moves[me[12].position_x][me[12].position_y] == 1){
                    return true;
                }
                
            }else{
                return true;
            }
            
            return false;
        }
        /**
         * 
         * @return function return true if game has been finished, false otherwise 
         */
        boolean isEnd(){
            
            if(player_switch){
                player_switch = false;
                return(isEnd_player(game.desk.player2.figurky , game.desk.player1.figurky));
            }else{
                player_switch = true;
                return(isEnd_player(game.desk.player1.figurky , game.desk.player2.figurky));
            }
        }
        
        /**
         *  function create a constuctor of selected game in GUI
         */
        
        void create_game() throws IOException, InterruptedException{
            if(act.data_from_GUI_menu_type_of_game == 1){
                if(!act.data_from_GUI_menu_time_acive){
                    game = new player_1_time(new game_desk());
                    if(!act.data_from_GUI_menu_color){
                        AI_move(game.desk.player1.figurky, game.desk.player2.figurky);
                    }                    
                }else{
                    game = new player_1(new game_desk());
                    if(!act.data_from_GUI_menu_color){
                        AI_move(game.desk.player1.figurky, game.desk.player2.figurky);
                    }                   
                }
            }
            if(act.data_from_GUI_menu_type_of_game == 2){
                if(!act.data_from_GUI_menu_time_acive){
                    game = new player_2_time(new game_desk());
                    //game.start_time(act.data_from_GUI_menu_time*60*1000,0,0);
                }else{
                    game = new player_2(new game_desk());
                    //game.start_time();                    
                }            
            }
            if(act.data_from_GUI_menu_type_of_game == 5){
                game = new set_desk(new game_desk());
            }
            if(act.data_from_GUI_menu_type_of_game == 3){
                game = new web_game(new game_desk());
                this.net = new com_net_thread(act, game, this.gui_frame, act.data_from_GUI_menu_color,LOGGER);
                this.net.start();
            }
        }
        /**
         * 
         * @param path_and_file - path of file with will be load
         * 
         * function create game of two plyer with out time and load data from file
         */
        void load_game(String path_and_file) throws FileNotFoundException, IOException{
            //System.out.println("game ha been load");
            try (FileInputStream fis = new FileInputStream(path_and_file)) {
                game = new player_2(new game_desk());
                game.move_iter = fis.read();
                for (int i=0; i < 16; i++){
                    
                    game.desk.player1.figurky[i].ID = fis.read();
                    game.desk.player1.figurky[i].position_x = fis.read();
                    game.desk.player1.figurky[i].position_y = fis.read();
                    game.desk.player1.figurky[i].active = 1 == fis.read();
                    game.desk.player1.figurky[i].first_move = 1 == fis.read();
                    
                    
                    game.desk.player2.figurky[i].ID = fis.read();
                    game.desk.player2.figurky[i].position_x = fis.read();
                    game.desk.player2.figurky[i].position_y = fis.read();
                    game.desk.player2.figurky[i].active = 1 == fis.read();
                    game.desk.player2.figurky[i].first_move = 1 == fis.read();
                    
                }
            }
        } 
        /**
         * 
         * @param path_and_file - destination and name of saving file
         * 
         * function save game of two players with out time to the file
         */
        void save_game(String path_and_file) throws FileNotFoundException, IOException{
            //System.out.println("game has been saved");
            
            try (FileOutputStream fos = new FileOutputStream(path_and_file)) {
                fos.write(game.move_iter);
                for (int i=0; i < 16; i++){
                    
                    fos.write(game.desk.player1.figurky[i].ID);
                    fos.write(game.desk.player1.figurky[i].position_x);
                    fos.write(game.desk.player1.figurky[i].position_y);
                    if(game.desk.player1.figurky[i].active){fos.write(1);}else{fos.write(0);}
                    if(game.desk.player1.figurky[i].first_move){fos.write(1);}else{fos.write(0);}
                    
                    fos.write(game.desk.player2.figurky[i].ID);
                    fos.write(game.desk.player2.figurky[i].position_x);
                    fos.write(game.desk.player2.figurky[i].position_y);
                    if(game.desk.player2.figurky[i].active){fos.write(1);}else{fos.write(0);}
                    if(game.desk.player2.figurky[i].first_move){fos.write(1);}else{fos.write(0);}
                    
                }
            }

                
                
                

        }
        
        /**
         *  function start treads of times
         */
        void start_time(){
            game.time.time.start();
            game.time.but.start();         
        
        }
        
        
        /*********************types of game******************************/
    /**
     *  basic class for types of game
     */
        class game_type{
            
            int move_iter = 0;
            game_desk desk;
            TIME time;
                   
            game_type(game_desk desk){
                this.desk = desk;
            }
            
            
            boolean move(int move_ID,int move_x,int move_y, int opt){
                if(this.move_iter%2 == 0){
                    if(move_ID > 15){
                        return false;
                    }

                    if(desk.player1.move(move_ID, desk.player2.figurky, desk.desk_string, move_x, move_y)){
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, true);
                        }
                        return true;
                    }
                    return false;

                }else{
                    if(move_ID < 16){
                        return false;
                    }

                    if(desk.player2.move(move_ID, desk.player1.figurky, desk.desk_string, move_x, move_y)){
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, false);
                        }                        
                        return true;
                    } 
                    return false;

                }
            }

            void start_time(long end_time, long time_player1, long time_player2){
                time = new TIME(end_time, time_player1 , time_player2, gui_frame, gui_frame.game_desk_menu.time2,gui_frame.game_desk_menu.time1,LOGGER);
            }
            
        }
        
    
        
        /************ extends classes *******************************/
        
        class player_1 extends game_type{

            player_1(game_desk desk){
                super(desk);
            }
            @Override
            boolean move(int move_ID,int move_x,int move_y, int opt){
                //System.out.println("move iter is : " + this.move_iter);
               
                if(act.data_from_GUI_menu_color){
                    if(move_ID > 15){
                        return false;
                    }

                    if(desk.player1.move(move_ID, desk.player2.figurky, desk.desk_string, move_x, move_y)){
                        //time.exchange();
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, true);
                        }
                        AI_move(desk.player2.figurky, desk.player1.figurky);
                        return true;
                    }
                    return false;

                }else{
                    if(move_ID < 16){
                        return false;
                    }

                    if(desk.player2.move(move_ID, desk.player1.figurky, desk.desk_string, move_x, move_y)){
                        //time.exchange();
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, false);
                        }
                        AI_move(desk.player1.figurky, desk.player2.figurky);
                        return true;
                    } 
                    return false;

                }
            }            
        }
        
        class player_1_time extends game_type{

            player_1_time(game_desk desk){
                super(desk);
            }
            
            @Override
            boolean move(int move_ID,int move_x,int move_y, int opt){
                //System.out.println("move iter is : " + this.move_iter);
               
                if(act.data_from_GUI_menu_color){
                    if(move_ID > 15){
                        return false;
                    }

                    if(desk.player1.move(move_ID, desk.player2.figurky, desk.desk_string, move_x, move_y)){
                        time.exchange();
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, true);
                        }
                        AI_move(desk.player2.figurky, desk.player1.figurky);
                        time.exchange();
                        return true;
                    }
                    return false;

                }else{
                    if(move_ID < 16){
                        return false;
                    }

                    if(desk.player2.move(move_ID, desk.player1.figurky, desk.desk_string, move_x, move_y)){
                        time.exchange();
                        this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, false);
                        }
                        
                        AI_move(desk.player1.figurky, desk.player2.figurky);
                        time.exchange();
                        return true;
                    } 
                    return false;

                }
            }

        }
        
        class player_2 extends game_type{

            player_2(game_desk desk){
                super(desk);
            }
        }
        
        class player_2_time extends game_type{
            
            player_2_time(game_desk desk){
                super(desk); 
            }  
        }
        
        class web_game extends game_type{
        
            web_game(game_desk desk){
                super(desk);
            }
            @Override
            boolean move(int move_ID,int move_x,int move_y, int opt){
                //System.out.println("move iter is : " + this.move_iter);
               
                if(this.move_iter%2 == 0){
                    if(move_ID > 15){
                        return false;
                    }
                    if(act.data_from_GUI_menu_color){
                        if(desk.player1.move(move_ID, desk.player2.figurky, desk.desk_string, move_x, move_y)){
                            if(act.data_from_GUI_menu_color){
                                String msg = move_ID + "a" + move_x + "a" + move_y;
                                net.out.println(msg);
                                gui_frame.game_desk_menu.time1.setText("White Player");
                                gui_frame.game_desk_menu.time2.setText("Balck Player - on move");                                  
                            }
                            this.move_iter++;
                            if(isEnd()){
                                JFrame dialog = new GUI_end_frame(gui_frame, true);
                            }                        
                            return true;
                        }
                    }    
                    return false;
                }else{
                    if(move_ID < 16){
                        return false;
                    }
                    if(!act.data_from_GUI_menu_color){
                        if(desk.player2.move(move_ID, desk.player1.figurky, desk.desk_string, move_x, move_y)){
                            if(!act.data_from_GUI_menu_color){
                                String msg = move_ID + "a" + move_x + "a" + move_y;
                                net.out.println(msg);
                                gui_frame.game_desk_menu.time1.setText("White Player - on move");
                                gui_frame.game_desk_menu.time2.setText("Balck Player");  
                            }
                            this.move_iter++;
                            if(isEnd()){
                                JFrame dialog = new GUI_end_frame(gui_frame, false);
                            }                        
                            return true;
                        }
                    }    
                    return false;

                }
            }            

             
        }
        
        class set_desk extends game_type{
            
            int[][] desk_int = new int[8][8];
        
            set_desk(game_desk desk){
                super(desk);
            }
            @Override
            boolean move(int move_ID,int move_x,int move_y, int opt){
                //System.out.println("move iter is : " + this.move_iter);
                
                
                
                if(!act.GUI_start_menu_isActive){   
                    if(this.move_iter%2 == 0){
                        if(move_ID > 15){
                            return false;
                        }

                        if(desk.player1.move(move_ID, desk.player2.figurky, desk.desk_string, move_x, move_y)){
                            
                            this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, true);
                        }                            
                            return true;
                        }
                        return false;

                    }else{
                        if(move_ID < 16){
                            return false;
                        }

                        if(desk.player2.move(move_ID, desk.player1.figurky, desk.desk_string, move_x, move_y)){
                            this.move_iter++;
                        if(isEnd()){
                            JFrame dialog = new GUI_end_frame(gui_frame, false);
                        }                            
                            return true;
                        } 
                        return false;

                    }
                }else{
                    //System.out.println("ahoj game");
                    if(opt == 1){
                        if(move_ID > 15){
                            game.desk.player2.figurky[move_ID-16].active = false;
                            return true;
                        }else{
                            game.desk.player1.figurky[move_ID].active = false;
                            return true;
                        }
                         
                    }else if(opt == 2){
                  
                            if(move_ID > 15){
                                game.desk.player2.figurky[move_ID-16].set_position(move_x, move_y);
                            }else{
                                game.desk.player1.figurky[move_ID].set_position(move_x, move_y);
                            }
                            return true;
                    } 
                }
                return false;
            }        
        }       
    }
