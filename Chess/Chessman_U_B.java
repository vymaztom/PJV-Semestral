
package chess;

/**
 * figure pawn
 * javadoc is same as chessman
 * @author vymaztom
 */
class Chessman_U_B extends Chessman {
 
    Chessman_U_B(int ID, int position_x, int position_y) {
        super(ID, position_x, position_y);
    }
    
    @Override
    boolean move(Chessman[] oposit, int[][] desk, int x, int y){
     
        int delta_x = x - this.position_x; 
        int delta_y = y - this.position_y;
        
        
        if(delta_y == 0 ){
            if(delta_x == 1){
                if(desk[x][y] == -1){  
                    this.first_move = false;
                    this.set_position(x, y);
                    return true;
                }
                return false;
            }else{
                if(delta_x == 2){
                    if((desk[x][y] == -1) && (desk[x-1][y] == -1) && this.first_move){
                        this.first_move = false;
                        this.set_position(x, y);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            
        }else{
            if(delta_x == 1){
                if((delta_y == 1 ) || (delta_y == -1)){
                    if(desk[x][y] != -1){
                        int che = desk[x][y];
                        if (this.ID < 16){
                            if(che > 15){
                                this.first_move = false;
                                oposit[che - 16].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){
                                this.first_move = false;
                                oposit[che].active = false;
                                this.set_position(x, y);
                                return true;
                            }
                            return false;
                        }
                    }
                    return false;    
                }
                return false;
            }
            return false;
        }
        
        

    }    
    
    @Override
    boolean posible_move(Chessman[] oposit, int[][] desk, int x, int y){
     
        /*
        
        xx 0x 1x 2x 3x 4x 5x 6x 7x
        0y xx xx xx xx xx xx xx xx
        1y xx xx xx xx xx xx xx xx
        2y xx xx xx xx xx xx xx xx
        3y xx xx xx xx xx xx xx xx
        4y xx xx xx xx xx xx xx xx
        5y xx xx xx xx xx xx xx xx
        6y xx xx xx xx xx xx xx xx
        7y xx xx xx xx xx xx xx xx
        xx 0x 1x 2x 3x 4x 5x 6x 7x
        
        
        */
        int delta_x = x - this.position_x; 
        int delta_y = y - this.position_y;
        
        
        if(delta_y == 0 ){
            if(delta_x == 1){
                if(desk[x][y] == -1){  

                    return true;
                }
                return false;
            }else{
                if(delta_x == 2){
                    if((desk[x][y] == -1) && (desk[x-1][y] == -1) && this.first_move){

                        return true;
                    }
                    return false;
                }
                return false;
            }
            
        }else{
            if(delta_x == 1){
                if((delta_y == 1 ) || (delta_y == -1)){
                    if(desk[x][y] != -1){
                        int che = desk[x][y];
                        if (this.ID < 16){
                            if(che > 15){


                                return true;
                            }
                            return false;
                        }else{
                            if(che < 16){


                                return true;
                            }
                            return false;
                        }
                    }
                    return false;    
                }
                return false;
            }
            return false;
        }
        
        

    }    
    
    
}
